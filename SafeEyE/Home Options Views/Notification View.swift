//
//  Notification View.swift
//  SafeEyE
//
//  Created by Hitarth on 28/01/20.
//  Copyright © 2020 com.example. All rights reserved.
//

import SwiftUI

struct Notification_View: View {
    var body: some View {
        GeometryReader { geo in
            ZStack {
                Color.init(#colorLiteral(red: 0.9921568627, green: 0.9176470588, blue: 0.9176470588, alpha: 1))
                    .edgesIgnoringSafeArea(.all)
                
                List(0..<5){ list in
                    ZStack(alignment: .leading) {
                        Color.init(#colorLiteral(red: 0.9921568627, green: 0.8352941176, blue: 0.8392156863, alpha: 1))
                        HStack(alignment: .center) {
                            
                            NotificationImage()
                                .frame(width: (geo.size.width)/7, height: (geo.size.height)/14)
                            
                            NotificationText()
                            
                        }.padding()
                    }.cornerRadius(20)
                    //.padding(.all,5)
                    
                    //List
                }.padding(.top)
                    .colorMultiply(Color.init(#colorLiteral(red: 0.9921568627, green: 0.9176470588, blue: 0.9176470588, alpha: 1)))
                //ZStack
            }
            //Geometry Reader
        }
    }
}



//MARK:- Notification Body.

struct NotificationImage: View {
    var body: some View {
        
        Image(systemName: "person.circle.fill")
            .resizable()
            .foregroundColor(Color.init(#colorLiteral(red: 0.2823529412, green: 0.1647058824, blue: 0.168627451, alpha: 1)))
            .padding(.leading)
    }
}


struct NotificationText: View {
    var body: some View {
        VStack(alignment: .leading) {
            
            Text("Asha")
                .font(.title)
                .fontWeight(.bold)
                .frame(alignment: .leading)
                .foregroundColor(Color.init(#colorLiteral(red: 0.2823529412, green: 0.1647058824, blue: 0.168627451, alpha: 1)))
            Text("This is great")
                .font(.headline)
                .fontWeight(.semibold)
                .foregroundColor(Color.init(#colorLiteral(red: 0.2823529412, green: 0.1647058824, blue: 0.168627451, alpha: 1)))
        }.padding(.leading)
    }
}

struct Notification_View_Previews: PreviewProvider {
    static var previews: some View {
        Notification_View()
    }
}
