//
//  SafeZone.swift
//  SafeEyE
//
//  Created by Hitarth on 19/01/20.
//  Copyright © 2020 com.example. All rights reserved.
//

import SwiftUI
import GoogleMaps
import CoreLocation

struct SafeZone: View {
    
    @State var alert = false
    @State var isButtonPressed = false
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    @State var timer = Timer()
    
    var body: some View {
        
        GeometryReader { geo in
            
            ZStack {
                
                MapView(alert: self.$alert, isButtonPressed: self.$isButtonPressed, timer: self.$timer).alert(isPresented: self.$alert) {
                    Alert(title: Text("Please Enable Location Service in Settings."))
                }
                
                
                VStack(alignment: .trailing){
                    Spacer()
                    HStack(alignment: .bottom){
                        Spacer()
                        Button(action: {
                            
                            self.isButtonPressed = true
                            
                        }, label: {
                            
                            
                            Image(systemName: "arrowtriangle.up.circle.fill")
                                .resizable()
                                .frame(width: (geo.size.width)/7.5, height: (geo.size.height)/15, alignment: .center)
                                .shadow(color: Color.init(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)), radius: 1, x: 2, y: 2)
                                .foregroundColor(.gray)
                        }).padding(.bottom,55)
                            .padding(.trailing,25)
                    }
                    
                } // Button Complete.
                
                //ZStack
            }.navigationBarTitle("Safe Zone")
                .navigationBarItems(leading: Button(action: {
                    
                    self.presentationMode.wrappedValue.dismiss()
                }, label: {
                    BackButton()
                        .frame(width: (geo.size.width)/11, height: (geo.size.height)/30, alignment: .center)
                }))
                .edgesIgnoringSafeArea(.all)
            
            
            //Geometry Reader
        }
    }
}



//MARK:- Setup Of MapView.

struct MapView: UIViewRepresentable {
    
    @ObservedObject var locationManager = LocationManager()
    @Binding var alert: Bool
    @Binding var isButtonPressed: Bool
    let marker: GMSMarker = GMSMarker()
    let mapView = GMSMapView()
    @Binding var timer: Timer
    
    
    func makeCoordinator() -> MapView.Coordinator {
        Coordinator(parent1: self)
    }
    
    func makeUIView(context: UIViewRepresentableContext<MapView>) -> GMSMapView {
        
        let camera: GMSCameraPosition =  GMSCameraPosition.camera(withLatitude: -33.86, longitude: 151.20, zoom: 1.0)
        mapView.animate(to: camera)
        
        return mapView
    }
    
    func updateUIView(_ uiView: GMSMapView, context: UIViewRepresentableContext<MapView>) {
        scheduledTimerWithTimeInterval()
    }
    
    func scheduledTimerWithTimeInterval(){
        timer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true, block: { (timer) in
            self.updateView()
        })
    }
    
    func updateView(){
        
        if let locationValue = self.locationManager.locationData?.coordinate {
            let camera = GMSCameraPosition(latitude: locationValue.latitude, longitude: locationValue.longitude, zoom: 10.0)
            
            if self.isButtonPressed{
                
                self.mapView.camera = camera
                self.mapView.isMyLocationEnabled.toggle()
                self.mapView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
                self.isButtonPressed = !self.isButtonPressed
                
            }
        }
    }
    
    
    class Coordinator: NSObject, CLLocationManagerDelegate {
        
        var parent: MapView
        
        init(parent1: MapView){
            parent = parent1
            
        }
        
        func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
            
            if status == .denied || status == .notDetermined || status == .restricted {
                parent.alert.toggle()
            }
        }
    }
    
}

struct SafeZone_Previews: PreviewProvider {
    static var previews: some View {
        SafeZone()
    }
}
