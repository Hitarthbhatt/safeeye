//
//  ContactView.swift
//  SafeEyE
//
//  Created by Hitarth on 15/01/20.
//  Copyright © 2020 com.example. All rights reserved.
//

import SwiftUI
import Firebase

struct ContactView: View {
    
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    @State var showSheet = false
    @ObservedObject var firebaseManager = FireBaseManager()
    
    var body: some View {
        GeometryReader { geo in
            
            ZStack {
                
                Color.init(#colorLiteral(red: 0.9921568627, green: 0.9176470588, blue: 0.9176470588, alpha: 1))
                    .edgesIgnoringSafeArea(.all)
                VStack {
                    List(self.firebaseManager.dataArray){ data in
                        
                        Button(action: {}, label: {
                            
                            ZStack{
                                HStack(alignment: .center) {
                                    
                                    Image(systemName: "person.circle.fill")
                                        .resizable()
                                        .foregroundColor(Color.init(#colorLiteral(red: 0.2823529412, green: 0.1647058824, blue: 0.168627451, alpha: 1)))
                                        .frame(width: (geo.size.width)/7, height: (geo.size.height)/14)
                                        .padding(.leading)
                                    ContactBody(data: data)
                              
                                }
                            }
                            
                        })
                        
                        //List
                    }
                    .onAppear(perform: {
                        self.firebaseManager.RetriveContactData()
                    })
                        .onDisappear(perform: {
                            self.firebaseManager.dataArray.removeAll()
                        })
                        .padding(.top)
                        .colorMultiply(Color.init(#colorLiteral(red: 0.9921568627, green: 0.9176470588, blue: 0.9176470588, alpha: 1)))
                }.frame(minHeight: 0, alignment: .center)
                
                
                VStack(alignment: .trailing){
                    Spacer()
                    HStack(alignment: .bottom){
                        Spacer()
                        Button(action: {
                            self.showSheet = true
                        }, label: {
                            
                            
                            Image(systemName: "plus.circle.fill")
                                .resizable()
                                .frame(width: (geo.size.width)/6, height: (geo.size.height)/12, alignment: .center)
                                .padding([.trailing,.bottom],30)
                                .foregroundColor(Color.init(#colorLiteral(red: 0.05098039216, green: 0.07058823529, blue: 0.337254902, alpha: 1)))
                                .shadow(color: Color.init(#colorLiteral(red: 0.05098039216, green: 0.07058823529, blue: 0.2706637408, alpha: 1)), radius: 1, x: 1, y: 1)
                            
                        }).sheet(isPresented: self.$showSheet) {
                            AddContact()
                        }
                    }
                    
                }
                //ZStack
            }.background(Color.init(#colorLiteral(red: 0.9882352941, green: 0.8745098039, blue: 0.8784313725, alpha: 1)))
                .navigationBarTitle("Contact")
                .navigationBarItems(leading:
                    
                    Button(action: {
                        
                        self.presentationMode.wrappedValue.dismiss()
                    }, label: {
                        BackButton()
                            .frame(width: (geo.size.width)/11, height: (geo.size.height)/30, alignment: .center)
                    })
            )
            
            //Geometry Reader
        }
    }
    
}

//TODO:- Back Button Setup.
struct BackButton: View {
    var body: some View {
        Image(systemName: "arrow.left")
            .resizable()
            .foregroundColor(Color.init(#colorLiteral(red: 0.05098039216, green: 0.07058823529, blue: 0.337254902, alpha: 1)))
            .padding(.leading,10)
            .shadow(color: Color.init(#colorLiteral(red: 0.05098039216, green: 0.07058823529, blue: 0.2706637408, alpha: 1)), radius: 1, x: 1, y: 1)
    }
}



//MARK:-  Add Contact Sheet.

struct AddContact: View {
    @State var contactName = ""
    @State var contactNumber = ""
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    var body: some View {
        GeometryReader { geo in
            
            ZStack {
                Color.init(#colorLiteral(red: 0.9921568627, green: 0.9176470588, blue: 0.9176470588, alpha: 1))
                    .edgesIgnoringSafeArea(.all)
                
                VStack {
                    TextField("Full Name", text: self.$contactName)
                        .padding()
                        .background(Color.init(#colorLiteral(red: 1, green: 0.8862745098, blue: 0.8862745098, alpha: 1)))
                        .cornerRadius(20)
                        .padding()
                    
                    TextField("Phone", text: self.$contactNumber)
                        .keyboardType(.numberPad)
                        .padding()
                        .background(Color.init(#colorLiteral(red: 1, green: 0.8862745098, blue: 0.8862745098, alpha: 1)))
                        .cornerRadius(20)
                        .padding()
                    
                    
                    //MARK:- Contact Saving Setup.
                    
                    Button(action: {
                        guard let uid = Auth.auth().currentUser?.uid else {return}
                        let reference = Database.database().reference(fromURL: "https://safeeye-dd1aa.firebaseio.com/")
                        let userReference = reference.child("Users").child(uid).child("Contact Detail")
                        let values = ["Name": self.contactName, "Number": self.contactNumber]
                        userReference.childByAutoId().setValue(values){
                            (error, reference) in
                            
                            if error != nil {
                                print(error!)
                            }else {
                                print("Contact Saved Successed!")
                                self.presentationMode.wrappedValue.dismiss()
                            }
                        }
                        
                    }, label: {
                        Image("save")
                            .resizable()
                            .renderingMode(.original)
                            .padding([.top, .bottom], 20)
                            .frame(width: (geo.size.width)/3, height: (geo.size.height)/7, alignment: .center)
                    })
                    //VStack
                }
                //ZStack
            }
            //Geometry Reader
        }
    }
}

//MARK:- Contact Body Setup.

struct ContactBody: View {
    
    var data: DataModel
    
    var body: some View {
        
        GeometryReader { geo in
            
            VStack(alignment: .leading) {
                
                Text(self.data.ContactName)
                    .font(.title)
                    .fontWeight(.bold)
                    .frame(alignment: .leading)
                    .foregroundColor(Color.init(#colorLiteral(red: 0.2823529412, green: 0.1647058824, blue: 0.168627451, alpha: 1)))
                Text(String(self.data.ContactNumber))
                    .font(.headline)
                    .fontWeight(.semibold)
                    .foregroundColor(Color.init(#colorLiteral(red: 0.2823529412, green: 0.1647058824, blue: 0.168627451, alpha: 1)))
                
            }.frame(width: geo.size.width)
            
        }
    }
}


struct ContactView_Previews: PreviewProvider {
    static var previews: some View {
        ContactView()
    }
}
