//
//  SoS Function.swift
//  SafeEyE
//
//  Created by Hitarth on 28/01/20.
//  Copyright © 2020 com.example. All rights reserved.
//

import Foundation
import SwiftUI
import Firebase

class SOSMessageSetup: ObservableObject {
    
    var sosMessage = "It's Great"
    
    //TODO:- Sending SOS Message To The FireBase
    func SendingMessageToFIR(){
        
        if Auth.auth().currentUser != nil {
            guard let uid = Auth.auth().currentUser?.uid else {return}
            let userReference = Database.database().reference().child("SOSData").child(uid).child("Message")
            let values = ["SOSMessage": sosMessage, "FromID": uid, "ToID": ""] as [String : Any]
            userReference.setValue(values) { (error, ref) in
                if error != nil {
                    print("Error in sending sos message")
                    return
                }
            }
        }
    }
    
}
