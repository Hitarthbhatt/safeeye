//
//  hello.swift
//  SafeEyE
//
//  Created by Hitarth on 06/02/20.
//  Copyright © 2020 com.example. All rights reserved.
//

import SwiftUI

struct hello: View {
    var body: some View {
        Text(/*@START_MENU_TOKEN@*/"Hello, World!"/*@END_MENU_TOKEN@*/)
            .font(.largeTitle)
    }
}

struct hello_Previews: PreviewProvider {
    static var previews: some View {
        hello()
    }
}
