//
//  ContentView.swift
//  SafeEyE
//
//  Created by Hitarth on 01/10/19.
//  Copyright © 2019 com.example. All rights reserved.
//

import SwiftUI
import Firebase

struct HomeView: View {
    
    @State var showMenu = false
    @State private var draggedOffSet = CGSize.zero
    
    var body: some View {
        
        GeometryReader { geo in
            ZStack {
                
                //MARK: - BackGround View Setting
                Image("HomeScreen")
                    .resizable()
                    .edgesIgnoringSafeArea(.all)
                    .frame(width: geo.size.width, height: geo.size.height)
                
                //MARK: - Set Main Buttons
                VStack(alignment: .center, spacing: (geo.size.height)/8){
                    
                    HStack(spacing: (geo.size.width - 30)/2) {
                        
                        NavigationLink(destination: ContactView().transition(.move(edge: .top))){
                            VStack {
                                ButtonIcon(Name: "Contacts")
                                    .frame(width: (geo.size.height)/10, height: (geo.size.height)/10, alignment: .center)
                                ButtonNames(Name: "Contact")
                            }
                        }
                        
                        NavigationLink(destination: SafeZone()){
                            
                            VStack {
                                ButtonIcon(Name: "SafeZone")
                                    .frame(width: (geo.size.height)/10, height: (geo.size.height)/10, alignment: .center)
                                ButtonNames(Name: "Safe Zone")
                            }
                        }
                        //HStack
                    }
                    
                    HStack(alignment: .center){
                        Button(action: {}, label: {
                            
                            ButtonIcon(Name: "sos")
                                .frame(width: (geo.size.height)/8, height: (geo.size.height)/8, alignment: .center)
                            
                        })
                        //HStack
                    }
                    
                    HStack(spacing: (geo.size.width - 30)/2){
                        Button(action: {}, label: {
                            
                            VStack {
                                ButtonIcon(Name: "Recording")
                                    .frame(width: (geo.size.height)/10, height: (geo.size.height)/10, alignment: .center)
                                ButtonNames(Name: "Recording")
                            }
                        })
                        
                        Button(action: {}, label: {
                            
                            VStack {
                                ButtonIcon(Name: "Rights")
                                    .frame(width: (geo.size.height)/10, height: (geo.size.height)/10, alignment: .center)
                                ButtonNames(Name: "Rights")
                            }
                        })
                        
                        //HStack
                    }
                    
                    //VStack
                }.frame(width: (geo.size.width))
                    .frame(minHeight: 0, maxHeight: geo.size.height, alignment: .top)
                    .position(x: (geo.size.width)/2, y: (geo.size.height)/1.5)
                .allowsHitTesting(!self.showMenu)
                
                HStack{
                    if self.showMenu{
                        MenuView()
                            .frame(width: (geo.size.width)/1.2, height: (geo.size.height))
                            .transition(.move(edge: .leading))
                            .offset(x: self.draggedOffSet.width)
                            .animation(.spring())
                            
                    }
                    Spacer()
                }
                
                
                //ZStack
            }
            .navigationBarHidden(self.showMenu)
                .navigationBarTitle("Home")
                .navigationBarItems(leading:
                    
                    //MARK :- Menu Bar Button
                    Button(action: {
                        
                        self.showMenu = !self.showMenu
                        
                    }, label: {
                        
                        Image("menu")
                            .resizable()
                            .renderingMode(.original)
                            .frame(width: (geo.size.width)/6.5, height: (geo.size.height)/15)
                        
                    })
                    
                    , trailing:
                    Button(action: {}, label: {
                        
                        Image("notification")
                            .resizable()
                            .renderingMode(.original)
                            .frame(width: (geo.size.width)/7, height: (geo.size.height)/13)
                    })
            )
                .gesture(DragGesture()
                    .onChanged({ (value) in
                        if value.translation.width <= 0 {
                            self.draggedOffSet = value.translation
                        }
                        
                    })
                    .onEnded({ (value) in
                        if value.translation.width < -100{
                            self.showMenu = false
                            self.draggedOffSet = CGSize.zero
                        }
                    }))
            
            //Geometry Reader
        }
    }
    

}



//MARK:- Buttons Icon And Name Setting.
struct ButtonNames: View {
    
    @State var Name = ""
    var body: some View {
        
        Text(Name)
            .fontWeight(.bold)
            .font(.system(size: 20))
            .foregroundColor(Color.init(#colorLiteral(red: 0.2823529412, green: 0.1647058824, blue: 0.168627451, alpha: 1)))
    }
}

struct ButtonIcon: View {
    
    @State var Name = ""
    var body: some View {
        
        Image(Name)
            .resizable()
            .renderingMode(.original)
            .shadow(color: Color.init(#colorLiteral(red: 0.2823529412, green: 0.1647058824, blue: 0.168627451, alpha: 1)), radius: 4, x: 0, y: 4)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView()
    }
}
