//
//  MenuView.swift
//  SafeEyE
//
//  Created by Hitarth on 12/12/19.
//  Copyright © 2019 com.example. All rights reserved.
//

import SwiftUI
import Firebase

struct MenuView: View {
    
    @State var isSignOut = false
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    var body: some View {
        
        GeometryReader { geo in
            
            ZStack {
                
                VStack(alignment: .leading, spacing: (geo.size.height)/18) {
                    
                    Button(action: {}, label: {
                        
                        VStack(alignment: .center) {
                            ProfileView(Name: "h.circle.fill")
                                .frame(width: (geo.size.width)/3, height: (geo.size.height)/9)
                            
                            Text("Profile Name")
                            .font(.title)
                            .fontWeight(.semibold)
                            .foregroundColor(Color.init(#colorLiteral(red: 0.7057705522, green: 0.3801456094, blue: 0.3882678151, alpha: 1)))
                            .padding(.leading, 20)
                            
                        }.frame(width: (geo.size.width)/1.8)
                        
                    })
                    
                    Divider().background(Color.init(#colorLiteral(red: 0.7057705522, green: 0.3801456094, blue: 0.3882678151, alpha: 1)))
                    
                    //TODO:- Recordings Data Setup.
                    Button(action: {}, label: {
                        
                        MenuOptionsIcon(Name: "doc.richtext")
                            .frame(width: (geo.size.width)/6, height: (geo.size.height)/30)
                        MenuOptionsName(Name: "Report")
                    })
                    
                    //TODO:- Report Option Setup.
                    Button(action: {}, label: {
                        
                        MenuOptionsIcon(Name: "video.circle.fill")
                            .frame(width: (geo.size.width)/6, height: (geo.size.height)/30)
                        MenuOptionsName(Name: "Recordings")
                    })
                    
                    //TODO:- Setting Button.
                    Button(action: {}, label: {
                        
                        MenuOptionsIcon(Name: "gear")
                            .frame(width: (geo.size.width)/6, height: (geo.size.height)/30)
                        MenuOptionsName(Name: "Settings")
                    })
                    
                    //MARK:- Log out the user and send them back to login view
                    NavigationLink(destination: LoginScreen_View(), isActive: self.$isSignOut){
                        Button(action: {
                            do {
                                try Auth.auth().signOut()
                                print("Sign Out")
                                self.isSignOut = true
                                self.presentationMode.wrappedValue.dismiss()
                            }catch { print("Error , There is an problem on logout") }
                            
                        }, label: {
                                MenuOptionsIcon(Name: "power")
                                    .frame(width: (geo.size.width)/6, height: (geo.size.height)/30)
                                
                                MenuOptionsName(Name: "Sign Out")
                        })
                    }
                    
                    Divider().background(Color.init(#colorLiteral(red: 0.7057705522, green: 0.3801456094, blue: 0.3882678151, alpha: 1)))
                    
                    //TODO:- Tell A Friend Button.
                    Button(action: {}, label: {
                        
                        MenuOptionsIcon(Name: "link")
                            .frame(width: (geo.size.width)/6, height: (geo.size.height)/30)
                        MenuOptionsName(Name: "Tell A Friend")
                    })
                    
                    //TODO:- Help and FeedBack Button.
                    Button(action: {}, label: {
                        
                        MenuOptionsIcon(Name: "questionmark.circle.fill")
                            .frame(width: (geo.size.width)/6, height: (geo.size.height)/30)
                        MenuOptionsName(Name: "Help and Feedback")
                    })
                    
                    //TODO:- About Button.
                    Button(action: {}, label: {
                        
                        MenuOptionsIcon(Name: "i.circle.fill")
                            .frame(width: (geo.size.width)/6, height: (geo.size.height)/30)
                        MenuOptionsName(Name: "About")
                    })
                    
                    //VStack
                }.frame(width: geo.size.width, height: geo.size.height, alignment: .leading)
                
                
                
                //ZStack
            }.background(Color.init(#colorLiteral(red: 0.9882352941, green: 0.8745098039, blue: 0.8784313725, alpha: 1)))
            .cornerRadius(20)
            .shadow(radius: 20)
            
        }
        
    }
}


//MARK:- Menu Options And Icons Settings.

struct MenuOptionsName: View {
    
    @State var Name = ""
    var body: some View {
        
        Text(Name)
            .font(.headline)
            .fontWeight(.semibold)
            .foregroundColor(Color.init(#colorLiteral(red: 0.7057705522, green: 0.3801456094, blue: 0.3882678151, alpha: 1)))
            .padding(.leading, 20)
    }
}

struct MenuOptionsIcon: View {
    
    @State var Name = ""
    var body: some View {
        
        Image(systemName: Name)
            .resizable()
            .imageScale(.small)
            .foregroundColor(Color.init(#colorLiteral(red: 0.7057705522, green: 0.3801456094, blue: 0.3882678151, alpha: 1)))
            .padding(.leading, 40)
    }
}


struct ProfileView: View {
    
    @State var Name = ""
    var body: some View {
        
        VStack {
            Image(systemName: Name)
                .resizable()
                .imageScale(.small)
                .foregroundColor(Color.init(#colorLiteral(red: 0.7057705522, green: 0.3801456094, blue: 0.3882678151, alpha: 1)))
                .shadow(color: Color.init(#colorLiteral(red: 0.2823529412, green: 0.1647058824, blue: 0.168627451, alpha: 1)), radius: 3, x: 1, y: 3)
                .padding(.leading, 20)
            //VStack
        }
    }
}



struct MenuView_Previews: PreviewProvider {
    static var previews: some View {
        MenuView()
    }
}
