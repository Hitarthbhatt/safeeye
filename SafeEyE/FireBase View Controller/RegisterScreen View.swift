//
//  RegisterScreen View.swift
//  SafeEyE
//
//  Created by Hitarth on 12/10/19.
//  Copyright © 2019 com.example. All rights reserved.
//

import SwiftUI
import Firebase

struct RegisterScreen_View: View {
    
    @State var userName: String = ""
    @State var contactNo: String = ""
    @State var userMail: String = ""
    @State var passWord: String = ""
    @State var isAuthSuccess = false
    @State var isBackPressed = false
    @State var offsetvalue = CGSize.zero
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    var body: some View {
        
        GeometryReader { geo in
            
            ZStack{
                
                //MARK:- Background Color Setting.
                Color.init(#colorLiteral(red: 0.9921568627, green: 0.9176470588, blue: 0.9176470588, alpha: 1))
                    .edgesIgnoringSafeArea(.all)
                
                VStack(spacing: (geo.size.height)/7) {
                    
                    VStack(alignment: .center) {
                        //MARK: - Set Logo.
                        Image("Icon")
                            .resizable()
                            .frame(width: (geo.size.width)/6, height: (geo.size.height)/10, alignment: .center)
                        //VStack
                    }
                    
                    
                    
                    VStack {
                        
                        //TODO:- Adding UserName TextField
                        TextField("Name", text: self.$userName)
                        .padding()
                        .background(Color.init(#colorLiteral(red: 1, green: 0.8862745098, blue: 0.8862745098, alpha: 1)))
                        .cornerRadius(20.0)
                        .padding()
                        
                        //TODO: - Adding User Contact Number TextField.
                        TextField("Number", text: self.$contactNo)
                            .keyboardType(.numberPad)
                            .padding()
                            .background(Color.init(#colorLiteral(red: 1, green: 0.8862745098, blue: 0.8862745098, alpha: 1)))
                            .cornerRadius(20.0)
                            .padding()
                        
                        //TODO: - Adding Email ID TextField.
                        TextField("Email ID", text: self.$userMail)
                            .padding()
                            .background(Color.init(#colorLiteral(red: 1, green: 0.8862745098, blue: 0.8862745098, alpha: 1)))
                            .cornerRadius(20.0)
                            .padding()
                        
                        //TODO: - Adding PassWord Secure Field.
                        SecureField("Password", text: self.$passWord)
                            .padding()
                            .background(Color.init(#colorLiteral(red: 1, green: 0.8862745098, blue: 0.8862745098, alpha: 1)))
                            .cornerRadius(20.0)
                            .padding()
                        
                        //TODO: - Sign Up Button.
                        NavigationLink(destination: HomeView(), isActive: self.$isAuthSuccess){
                            Button(action: {
                                
                                //TODO:- Log in to the user
                                Auth.auth().createUser(withEmail: self.userMail, password: self.passWord) { (user, error) in
                                    
                                    if error != nil {
                                        print("There is an error in Sign Up")
                                        return
                                    }
                                    print("Successful!")
                                    
                                    //MARK:- Setup user database.
                                    
                                    guard let uid = user?.user.uid else { return }
                                    let reference = Database.database().reference(fromURL: "https://safeeye-dd1aa.firebaseio.com/")
                                    let userReference = reference.child("Users").child(uid)
                                    let values = ["Name": self.userName, "Number": self.contactNo, "Email": self.userMail]
                                    userReference.updateChildValues(values) { (error, ref) in
                                        if error != nil {
                                            print("Error in saving register values")
                                            return
                                        }
                                         self.isAuthSuccess = true
                                    }
                                }
                                
                            }){
                                Image("Sign Up")
                                    .resizable()
                                    .renderingMode(.original)
                                    .padding([.top, .bottom], 20)
                                    .frame(width: (geo.size.width)/3, height: (geo.size.height)/7, alignment: .center)
                            }
                        }
                        
                        
                        //VStack
                    }.frame(minHeight: 0, maxHeight: (geo.size.height))
                        .position(x: (geo.size.width)/2, y: (geo.size.height)/5)
                    
                    //VStack
                }
                
                //ZStack
            }.navigationBarTitle(Text("Sign Up"))
                .navigationBarItems(leading:
                    Button(action: {
                        self.presentationMode.wrappedValue.dismiss()
                    }, label: {
                        
                        BackButton()
                        .frame(width: (geo.size.width)/11, height: (geo.size.height)/30, alignment: .center)
                        
                    })
                    
                    
            )
            //Geometry Reader
        }
        
    }
}


struct RegisterScreen_View_Previews: PreviewProvider {
    static var previews: some View {
        RegisterScreen_View()
    }
}
