//
//  LoginScreen View.swift
//  SafeEyE
//
//  Created by Hitarth on 12/10/19.
//  Copyright © 2019 com.example. All rights reserved.
//

import SwiftUI
import Firebase

struct LoginScreen_View: View {
    
    @State var userName: String = ""
    @State var passWord: String = ""
    @State var isCreatePressed = false
    @State var isAuthSuccess = false
    @State var showProgress = false
    @State var appeared = 0.0
    
    var body: some View {
        GeometryReader { geo in
            
            NavigationView {
                
                ZStack{
                    
                    //MARK:- Background Color Setting.
                    Color.init(#colorLiteral(red: 0.9921568627, green: 0.9176470588, blue: 0.9176470588, alpha: 1))
                        .edgesIgnoringSafeArea(.all)
                    
                    VStack(spacing: (geo.size.height)/7) {
                        //MARK: - Welcome Text.
                        VStack {
                            
                            
                            //MARK: - Set Logo.
                            Image("Icon")
                                .resizable()
                                .frame(width: (geo.size.width)/6, height: (geo.size.height)/10, alignment: .center)
                            //VStack
                        }
                        
                        
                        
                        VStack {
                            //MARK: - Adding UserName TextField.
                            TextField("UserName", text: self.$userName)
                                .padding()
                                .background(Color.init(#colorLiteral(red: 1, green: 0.8862745098, blue: 0.8862745098, alpha: 1)))
                                .cornerRadius(20.0)
                                .padding()
                            
                            //MARK: - Adding PassWord Secure Field.
                            SecureField("Password", text: self.$passWord)
                                .padding()
                                .background(Color.init(#colorLiteral(red: 1, green: 0.8862745098, blue: 0.8862745098, alpha: 1)))
                                .cornerRadius(20.0)
                                .padding()
                            
                            //MARK: - Login Button.
                            
                            NavigationLink(destination: HomeView(), isActive: self.$isAuthSuccess){
                                
                                Button(action: {
                                    
                                    //TODO:- Log in to the user
                                    Auth.auth().signIn(withEmail: self.userName, password: self.passWord) { (user, error) in
                                        
                                        if error != nil {
                                            print("There is an error in Sign In")
                                            
                                        }else {
                                            print("Successful!")
                                            self.isAuthSuccess = true
                                        }
                                    }
                                    
                                }){
                                    Image("Login")
                                        .resizable()
                                        .renderingMode(.original)
                                        .padding([.top, .bottom], 20)
                                        .frame(width: (geo.size.width)/3, height: (geo.size.height)/7, alignment: .center)
                                }
                                
                            }
                            
                            
                            
                            //MARK: - Create New Account Button
                            NavigationLink(destination: RegisterScreen_View(), label: {
                                
                                Text("Create New Account")
                                    .font(.title)
                                    .fontWeight(.semibold)
                                    .foregroundColor(Color.gray)
                                    .padding([.top], 20)
                                
                            })
                            
                            //VStack
                        }.frame(minHeight: 0, maxHeight: (geo.size.height))
                            .position(x: (geo.size.width)/2, y: (geo.size.height)/5)
                        
                        //VStack
                    }
                    
                    //ZStack
                }
                .navigationBarTitle("Welcome", displayMode: .inline)
                .navigationBarBackButtonHidden(true)
                
                //Navigation View
            }.statusBar(hidden: true)
                .opacity(self.appeared)
                .animation(Animation.easeInOut(duration: 1.0), value: self.appeared)
            .onAppear {self.appeared = 1.0}
            .onDisappear {self.appeared = 0.0}
            
            //Geometry Reader
        }
        
    }
    
    
    
}



extension UINavigationController {
    
    override open func viewDidLoad() {
        super.viewDidLoad()
        
        let standerdAppearance = UINavigationBarAppearance()
        standerdAppearance.configureWithTransparentBackground()
        standerdAppearance.titlePositionAdjustment = UIOffset(horizontal: 0,vertical: 5)
        standerdAppearance.titleTextAttributes = [
            //NSAttributedString.Key.font : UIFont.preferredFont(forTextStyle: .largeTitle),
            NSAttributedString.Key.foregroundColor : UIColor.darkGray
        ]
        
        navigationBar.standardAppearance = standerdAppearance
    }
    
}

struct LoginScreen_View_Previews: PreviewProvider {
    static var previews: some View {
        LoginScreen_View()
    }
}
