//
//  Location Manager.swift
//  SafeEyE
//
//  Created by Hitarth on 28/01/20.
//  Copyright © 2020 com.example. All rights reserved.
//

import SwiftUI
import Firebase
import CoreLocation


//MARK:- Location Manager Class For Getting User Location Data.
class LocationManager: NSObject, ObservableObject {
    
    @Published var locationData: CLLocation?
    
    override init(){
        super.init()
        
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyKilometer
        self.locationManager.requestWhenInUseAuthorization()
        self.locationManager.requestAlwaysAuthorization()
        self.locationManager.startUpdatingLocation()
    }
    
    private let locationManager = CLLocationManager()
}

extension LocationManager: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let location = locations[locations.count-1]
        self.locationData = location
    }
}
