//
//  Firebase Manager.swift
//  SafeEyE
//
//  Created by Hitarth on 28/01/20.
//  Copyright © 2020 com.example. All rights reserved.
//

import SwiftUI
import Firebase
import CoreLocation


struct DataModel: Identifiable {
    var id = UUID()
    var ContactName: String = ""
    var ContactNumber: Int = 0
}

struct locationData {
    
    var UserID = ""
    var Latitude = 0
    var Longitude = 0
}


//MARK:- Firebase Manager Class.
class FireBaseManager: ObservableObject {
    
    @Published var dataArray = [DataModel]()
    
    //TODO:- Updating location to the firebase server.
    func UpdateLocationToFIR(latitude: CLLocationDegrees, longitude: CLLocationDegrees){
        
        if Auth.auth().currentUser != nil {
            guard let uid = Auth.auth().currentUser?.uid else {return}
            let userReference = Database.database().reference().child("SOSData").child(uid).child("Location")
            
            let values = ["Latitude": latitude, "Longitude": longitude] as [String : Any]
            
            userReference.updateChildValues(values) { (error, ref) in
                if error != nil {
                    print("Error in updating location on server")
                    return
                }
            }
        }
        
    }
    
    
    //TODO:- Retriving Contact Data from the firebase server.
    func RetriveContactData() {
        
        guard let uid = Auth.auth().currentUser?.uid else {return}
        let reference = Database.database().reference()
        let userReference = reference.child("Users").child(uid).child("Contact Detail")
        
        userReference.observe(.childAdded, with: { (Snapshot) in
            let snapShotValue = Snapshot.value as! Dictionary<String,String>
                if let Number = snapShotValue["Number"],let name = snapShotValue["Name"] {
                    var data = DataModel()
                    data.ContactName = name
                    data.ContactNumber = Int(Number)!
                    self.dataArray.append(data)
                }
        })
    }
    
    
}
