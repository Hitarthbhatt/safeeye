//
//  AppDelegate.swift
//  SafeEyE
//
//  Created by Hitarth on 01/10/19.
//  Copyright © 2019 com.example. All rights reserved.
//

import UIKit
import SwiftUI
import Firebase
import GoogleMaps
import GooglePlaces
import CoreLocation

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var timer = Timer()
    @ObservedObject var firebaseManager = FireBaseManager()
    @ObservedObject var locationManager = LocationManager()
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        GMSServices.provideAPIKey("AIzaSyA1Zh2OHiZgjKH5HHJK45SCnnk8a5aODMs")
        GMSPlacesClient.provideAPIKey("AIzaSyA1Zh2OHiZgjKH5HHJK45SCnnk8a5aODMs")
        
        scheduledTimerWithTimeInterval()
        
        return true
    }
    
    //MARK:- Updating User Location On The Firebase Server.
    func scheduledTimerWithTimeInterval(){
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.updateLocation), userInfo: nil, repeats: true)
    }
    
    @objc func updateLocation(){
        if let updatingLocation = self.locationManager.locationData?.coordinate{
            self.firebaseManager.UpdateLocationToFIR(latitude: updatingLocation.latitude, longitude: updatingLocation.longitude)
        }
    }
    
    // MARK: UISceneSession Lifecycle
    
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }
    
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
    
    
}

