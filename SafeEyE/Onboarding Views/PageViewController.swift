//
//  PageViewController.swift
//  SafeEyE
//
//  Created by Hitarth on 29/10/19.
//  Copyright © 2019 com.example. All rights reserved.
//

import Foundation
import SwiftUI
import UIKit

struct PageViewController: UIViewControllerRepresentable {
    
    var viewControllers: [UIViewController]
    
    @Binding var currentPageIndex : Int
    
    
    //MARK: - UIView Controller Representable Protocol Method
    
    /*The UIViewControllerRepresentable protocol has three mandatory methods:
     
     1. makeUIViewController: This method is used for creating the UIViewController we want to present.
     
     2. updateUIViewController: This method updates the UIViewController to the latest configuration every time it gets called .
     
     3. makeCoordinator: This method initialises a Coordinator which serves as a kind of a servant for handling delegate and datasource patterns and user inputs. We will talk about this in more detail later. */
    
    func makeCoordinator() -> PageViewController.Coordinator {
        Coordinator(self)
    }
    
    func makeUIViewController(context: Context) -> UIPageViewController {
        let pageViewController = UIPageViewController(transitionStyle: .scroll, navigationOrientation: .horizontal)
        
        pageViewController.dataSource = context.coordinator
        pageViewController.delegate = context.coordinator
        
        return pageViewController
    }
    
    func updateUIViewController(_ pageViewController: UIPageViewController, context: Context) {
        pageViewController.setViewControllers([viewControllers[currentPageIndex]], direction: .forward, animated: true)
    }
    
    
    //MARK: -  Coordinator SubClass
    
    class Coordinator: NSObject, UIPageViewControllerDataSource, UIPageViewControllerDelegate{
        
        var parent: PageViewController
        
        init(_ pageViewController: PageViewController) {
            self.parent = pageViewController
        }
        
        func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
            //retrieves the index of the currently displayed view controller
            guard let index = parent.viewControllers.firstIndex(of: viewController) else {
                return nil
            }
            
            //shows the last view controller when the user swipes back from the first view controller
            if index == 0 {
                return nil
            }
            
            //show the view controller before the currently displayed view controller
            return parent.viewControllers[index - 1]
            
        }
        
        func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
            
            //retrieves the index of the currently displayed view controller
            guard let index = parent.viewControllers.firstIndex(of: viewController) else {
                return nil
            }
            //shows the first view controller when the user swipes further from the last view controller
            if index + 1 == parent.viewControllers.count {
                return nil
            }
            //show the view controller after the currently displayed view controller
            return parent.viewControllers[index + 1]
            
        }
        
        func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
            
            if completed,
                let visibleViewController = pageViewController.viewControllers?.first,
                let index = parent.viewControllers.firstIndex(of: visibleViewController) {
                
                parent.currentPageIndex = index
            }
            
        }
    }
    
}
