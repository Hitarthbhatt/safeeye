//
//  SubViews.swift
//  SafeEyE
//
//  Created by Hitarth on 29/10/19.
//  Copyright © 2019 com.example. All rights reserved.
//

import SwiftUI

struct SubViews: View {
    
    var imageString: String
    
    var body: some View {
        
        VStack {
            
            Image(imageString)
                .resizable()
                .edgesIgnoringSafeArea(.all)
            
        }
        
    }
}

struct OnBoardingView_Previews: PreviewProvider {
    static var previews: some View {
        SubViews(imageString: "view1")
    }
}
