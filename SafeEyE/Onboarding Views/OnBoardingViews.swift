//
//  OnBoardingViews.swift
//  SafeEyE
//
//  Created by Hitarth on 29/10/19.
//  Copyright © 2019 com.example. All rights reserved.
//

import SwiftUI

struct OnBoardingViews: View {
    
    //MARK: - Calling All Views
    var subViews = [
        //UIHostingControllers are used for wrapping SwiftUI views in order to use them within the UIKit framework.
        
        UIHostingController(rootView: SubViews(imageString: "view1")),
        UIHostingController(rootView: SubViews(imageString: "view2")),
        UIHostingController(rootView: SubViews(imageString: "view3"))
    ]
    
    var titles = ["SafeEyE", "SoS", "Real-Time Gps"]
    var titleColors = [Color.init(#colorLiteral(red: 0.9803921569, green: 0.7254901961, blue: 0.2549019608, alpha: 1)), Color.init(#colorLiteral(red: 0.1568627451, green: 0.3921568627, blue: 0.9411764706, alpha: 1)), Color.init(#colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1))]
    
    var captions = ["Destroy Everything That Destroys You", "Sos button directly alerts authorities in case of emergency", "Real-Time GPS System To Track You Anywhere"]
    var captionColors = [Color.init(#colorLiteral(red: 0.8196078431, green: 0.8196078431, blue: 0.8666666667, alpha: 1)), Color.init(#colorLiteral(red: 0.8196078431, green: 0.8196078431, blue: 0.8666666667, alpha: 1)), Color.init(#colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1))]
    
    @State var currentPageIndex = 0
    @State var isStartPressed = false
    
    var body: some View {
        
        GeometryReader { geo in
            
            ZStack {
                ZStack {
                    PageViewController(viewControllers: self.subViews, currentPageIndex: self.$currentPageIndex)
                        .edgesIgnoringSafeArea(.all)
                    
                    VStack {
                        
                        VStack {
                            
                            Text(self.titles[self.currentPageIndex])
                                .font(.largeTitle)
                                .fontWeight(.bold)
                                .foregroundColor(self.titleColors[self.currentPageIndex])
                                .position(x: (geo.size.width)/2, y: (geo.size.height)/1.8)
                            
                            Text(self.captions[self.currentPageIndex])
                                .font(.title)
                                .frame(width: 300, height: 150, alignment: .center)
                                .foregroundColor(self.captionColors[self.currentPageIndex])
                                .position(x: (geo.size.width)/2, y: (geo.size.height)/2)
                                .lineLimit(3)
                            
                            //VStack
                        }
                        
                        
                        HStack {
                            pageControl(numberOfPages: self.subViews.count, currentPageIndex: self.$currentPageIndex)
                                .padding([.leading],40)
                            
                            Spacer()
                            
                            Button(action: {
                                
                                if self.currentPageIndex+1 != self.subViews.count {
                                    self.currentPageIndex += 1
                                }
                                else if self.currentPageIndex+1 == self.subViews.count {
                                    
                                    let userDefaults = UserDefaults.standard
                                    userDefaults.set(true, forKey: "onBoardingComplete")
                                    userDefaults.synchronize()
                                    
                                    self.isStartPressed = true
                                    
                                }
                                
                            }, label: {
                                if self.currentPageIndex+1 == self.subViews.count {
                                    
                                    Image("start")
                                        .resizable()
                                        .renderingMode(.original)
                                        .frame(width: (geo.size.width)/5, height: (geo.size.height)/16)
                                    
                                }else {
                                    Image(systemName: "arrow.right")
                                        .resizable()
                                        .frame(width: 30, height: 30)
                                        .foregroundColor(Color.white)
                                        .padding()
                                        .background(Color.orange)
                                        .cornerRadius(30)
                                }
                                
                                
                            }).padding([.trailing],20)
                            
                            //HStack
                        }.frame(maxHeight: geo.size.height, alignment: .bottom)
                        
                        //VStack
                    }
                    
                    //ZStack
                }
                
                if self.isStartPressed {
                    
                    LoginScreen_View()
                }
                
                //ZStack
            }.animation(.easeIn(duration: 0.4))
            //GeometryReader
        }
    }
}



struct OnBoardingViews_Previews: PreviewProvider {
    static var previews: some View {
        OnBoardingViews()
    }
}
