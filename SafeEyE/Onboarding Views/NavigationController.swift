//
//  NavigationController.swift
//  SafeEyE
//
//  Created by Hitarth on 08/01/20.
//  Copyright © 2020 com.example. All rights reserved.
//

import Foundation
import SwiftUI
import UIKit

class Navigation: UINavigationController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    var navigationBarAppearance = UINavigationBarAppearance()
    
    func toTheRoot() {
        
        navigationController?.popToRootViewController(animated: true)
    }
}
