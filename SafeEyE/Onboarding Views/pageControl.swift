//
//  pageControl.swift
//  SafeEyE
//
//  Created by Hitarth on 24/12/19.
//  Copyright © 2019 com.example. All rights reserved.
//

import Foundation
import UIKit
import SwiftUI

struct pageControl: UIViewRepresentable {
    
    var numberOfPages: Int
    
    @Binding var currentPageIndex: Int
    
    func makeUIView(context: Context) -> UIPageControl {
        
        let control = UIPageControl()
        
        control.numberOfPages = numberOfPages
        control.currentPageIndicatorTintColor = .orange
        control.pageIndicatorTintColor = .gray
        
        return control
    }
    
    func updateUIView(_ uiView: UIPageControl, context: Context) {
        
        uiView.currentPage = currentPageIndex
        
    }
    
}
